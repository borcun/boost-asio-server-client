/**
 * @file common.h
 * @author B. Orcun OZKABLAN
 * 
 * The file includes definitions related to server-client communication
 */

#ifndef COMMON_H
#define COMMON_H

//! debug flag to print debug log 0: no print, 1: print
#define DEBUG_FLAG (0)

//! invalid IP address
#define INVALID_IP_ADDRESS ("")

//! invalid port number
#define INVALID_PORT_NUMBER (0U)

//! minimum IPv4 length
#define MIN_IPV4_LENGTH (7U)

//! maximum IPv4 length
#define MAX_IPV4_LENGTH (15U)

//! invalid transaceive indicator
#define INVALID_TRANSCEIVE (-1)

//! common transceive size
#define TRANSCEIVE_SIZE (4)

//! ACK from server to client
#define TCP_SERVER_ACK (0xFFFFFFFFU)

//! transceive period
#define TRANSCEIVE_PERIOD (50U)

//! transceive delay in ms
#define TRANSCEIVE_DELAY (10U)

//! main loop in server in ms
#define SERVER_MAIN_LOOP (10U)

//! minimum data limit (10^6)
#define MIN_DATA_LIMIT (0x000F4240U)

//! maximum data limit (10^8)
#define MAX_DATA_LIMIT (0x05F5E100U)

//! sum limit to end transceive between server and client(s)
#define TRANSCEIVE_SUM_LIMIT (0xFFFFFFFFULL)

#endif
