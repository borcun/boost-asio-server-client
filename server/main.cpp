#include "tcp_server.h"
#include "accumulator.h"
#include "common.h"
#include <iostream>
#include <boost/chrono.hpp>
#include <boost/thread/thread.hpp>

//! server state
bool is_server_closed = false;

/**
 * @brief function that accepts any connection request to server from client
 * @param server - server connection point
 * @return -
 */
void acceptConnection(TCPServer * const server) {
  // during server is open, accept requests from clients
  while (!is_server_closed) {
    server->accept();
  }

  return;
}

/**
 * @brief function that displays application usage
 * @return -
 */
void usage(void) {
  std::cout << "usage: ./tcp_server <port>" << std::endl;

  return;
}

int main(int argc, char **argv) {
  if (2 != argc) {
    usage();
    return -1;
  }

  TCPServer *server = TCPServer::getInstance();

  /**
   * @attention if the converted value with using atoi would be out of the range of 
   * representable values by an int, it causes undefined behavior. Thus, there is still
   * better options, but it is OK for now.
   */
  server->setPort(atoi(argv[1]));
    
  if (!server->start()) {
    std::cerr << "Could not start server" << std::endl;
    return -1;
  }

  std::cout << "Server is running..." << std::endl;

  // run a thread to handle next connection request when processing other operations.
  boost::thread acceptorThread(boost::bind(acceptConnection, server));

  //  the below condition finishes whole process between server and client(s)
  while (TRANSCEIVE_SUM_LIMIT > Accumulator::getData()) {
    boost::this_thread::sleep_for(boost::chrono::milliseconds(SERVER_MAIN_LOOP));
  }

  std::cout << "SUM exceeds limit " << TRANSCEIVE_SUM_LIMIT << std::endl;
  
  is_server_closed = true;
  
  /**
   * @attention normally, join should be preferred, but acceptor blocks the thread.
   * Thus, it is temporarily solution, but not best
   */
  acceptorThread.interrupt();

  server->stop();

  std::cout << "Server is stopped." << std::endl;

  return 0;
}
