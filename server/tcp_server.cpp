#include "common.h"
#include "accumulator.h"
#include "tcp_server.h"
#include <boost/system/error_code.hpp>
#include <iostream>

TCPServer *TCPServer::m_self = nullptr;

TCPServer::TCPServer(void) :
  m_port(INVALID_PORT_NUMBER),
  m_is_started(false)
{
}

TCPServer::~TCPServer() {
  if (nullptr != m_self) {
    delete m_self;
  }

  m_self = nullptr;
}

TCPServer *TCPServer::getInstance(void) {
  if (nullptr == m_self) {
    m_self = new TCPServer();
  }

  return m_self;
}

void TCPServer::setPort(const unsigned short port) {
  if (INVALID_PORT_NUMBER == port) {
    std::cerr << "Invalid port number" << std::endl;
  }
  else if (m_is_started) {
    std::cerr << "Could not set port when server is running" << std::endl;
  }
  else {
    m_port = port;
  }

  return;
}

unsigned short TCPServer::getPort(void) const {
  return m_port;
}

bool TCPServer::start(void) {
  if (m_is_started) {
    std::cerr << "The server was already started" << std::endl;
    
    return false;
  }

  if (INVALID_PORT_NUMBER == m_port) {
    std::cerr << "Invalid port number" << std::endl;
    
    return false;
  }
  
  m_is_started = true;
  // set sum with 0 at beginning of process
  Accumulator::setData(0);
  
  return m_is_started;
}

void TCPServer::stop(void) {  
  if (0 != m_conn_objs.size()) {
    for (unsigned int i = 0; i < m_conn_objs.size(); ++i) {
      // disconnect
      m_conn_objs[i]->is_connected = false;
      m_conn_objs[i]->thr->join();
      delete m_conn_objs[i]->thr;
      
      // close sockets
      m_conn_objs[i]->socket->close();
      delete m_conn_objs[i]->socket;
      delete m_conn_objs[i];
    }

    m_conn_objs.clear();
  }

  m_is_started = false;

  return;
}

void TCPServer::accept(void) {  
  if (!m_is_started) {
    std::cerr << "There is no starting server" << std::endl;
    return;
  }

  ConnectionObject *conn_obj = new ConnectionObject();
  ip::tcp::acceptor acceptor(m_context, ip::tcp::endpoint(ip::tcp::v4(), m_port));
  boost::system::error_code ec;

  if (nullptr == conn_obj) {
    std::cerr << "Could not create connection object" << std::endl;
    return;
  }
  
  conn_obj->socket = new ip::tcp::socket(m_context);

  if (nullptr == conn_obj->socket) {
    std::cerr << "Could not create socket for TCP client" << std::endl;
    delete conn_obj;
    
    return;
  }

  // accept connection from client, then handle the connection with socket instance
  acceptor.accept(*(conn_obj->socket), &ec);

  if (boost::system::errc::success != ec.value()) {
    std::cerr << ec.message() << std::endl;
    delete conn_obj->socket;
    delete conn_obj;
    
    return;
  }

  // get client information
  conn_obj->endpoint = conn_obj->socket->remote_endpoint(ec);

  if (boost::system::errc::success != ec.value()) {
    std::cout << "New connection [Unknown client]" << std::endl;
  }
  else {
    std::cout << "New connection [" << conn_obj->endpoint << "]" << std::endl;
  }

  // create a thread for client connection
  conn_obj->context = &m_context;
  conn_obj->is_connected = true;
  conn_obj->thr = new boost::thread(boost::bind(connectionHandler, conn_obj));

  if (nullptr == conn_obj->thr) {
    std::cerr << "Could not create thread function for TCP client" << std::endl;
    delete conn_obj->socket;
    delete conn_obj;
    
    return;
  }

  // add connection objects into the list
  m_conn_objs.push_back(conn_obj);

  return;
}
