/**
 * @file connection_object.h
 * @author B. Orcun OZKABLAN
 *
 * The file contains connection object structure and some I/O functions about receive and send
 */

#ifndef CONNECTION_OBJECT_H
#define CONNECTION_OBJECT_H

#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>

using namespace boost::asio;

/**
 * @class ConnectionObject that contains field related to server connection object
 */
class ConnectionObject {
 public:
  //! io context reference to use for async operations
  io_context *context;

  //! socket that is dedicated for client connection
  ip::tcp::socket *socket;

  //! thread reference
  boost::thread *thr;

  //! endpoint information
  ip::tcp::endpoint endpoint;

  //! connection state
  bool is_connected;
};

//! mutex to sync thread when accessing sum variable
static boost::mutex sum_mutex;

/**
 * @brief connection handler function that is used by each connection opened for a client
 * @param conn_obj - connection object reference
 * @return -
 */
void connectionHandler(ConnectionObject *conn_obj);

/**
 * @brief send handler function that is called after sending is done
 * @param ec - error code
 * @param send_count - send count
 * @return -
 */
void sendHandler(const boost::system::error_code &ec, std::size_t send_count);

/**
 * @brief receive handler function that is called after receiving is done
 * @param ec - error code
 * @param recv_count - receive count
 * @return -
 */
void receiveHandler(const boost::system::error_code &ec, std::size_t recv_count);

#endif
