/**
 * @file accumulator.h
 * @author B. Orcun OZKABLAN
 *
 * The accumulator class that manages operations related to data from client(s)
 */

#ifndef ACCUMULATOR_H
#define ACCUMULATOR_H

/**
 * @class Accumulator
 */
class Accumulator {
 public:
  /**
   * @brief default constructor
   */
  Accumulator(void);

  /**
   * @brief destructor
   */
  virtual ~Accumulator();

  /**
   * @brief function that sets accumulator data
   * @param data - data to be set
   * @return -
   */
  static void setData(const unsigned long int data);

  /**
   * @brief function that gets accumulator data
   * @return accumulator data
   */
  static unsigned long int getData(void);

  /**
   * @brief function that accumulates data in member of class
   * @remark function should be protected with any protection mechanism such as mutex
   * @param data - data to be accumulated
   * @return -
   */
  static void accumulate(const unsigned long int data);

 private:
  //! accumulator data, (a.k.a SUM in the job description)
  static unsigned long int m_data;
};

#endif
